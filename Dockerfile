FROM alpine:3.9
MAINTAINER Gokhan Sari "gokhan@gokhansari.me"

RUN apk update \
    && apk add --virtual build-deps gcc python3-dev dos2unix \
    && apk add bash python3 postgresql-dev

WORKDIR /unicorn

# add user with /webgazer home directory and set permissions
RUN adduser -H -h /unicorn -D unicorn unicorn
RUN chown -R unicorn:unicorn /unicorn

# copy python requirements list
COPY requirements.txt requirements.txt

# install python dependencies
RUN pip3 install -r ./requirements.txt

# delete build dependencies
RUN apk del build-deps

# copy files
COPY unicorn unicorn
COPY manage.py manage.py

RUN python3 ./manage.py collectstatic --no-input

# delete unnecessary files
RUN rm -rf requirements.txt

ENTRYPOINT ["docker-entrypoint.sh"]
