from django.db import models

from django.contrib.auth.models import AbstractBaseUser

from .querysets import UserManager


class User(AbstractBaseUser):
    email = models.EmailField(
        unique=True,
        error_messages={
            'unique': "There is already a user with this e-mail address.",
        },
        verbose_name="E-mail address",
    )

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ()

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, *_, **__):
        return True

    def has_module_perms(self, *_, **__):
        return True

    @property
    def is_staff(self):
        return True
