from django.core.exceptions import ValidationError
from rest_framework.decorators import (
    api_view,
    permission_classes,
)
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status

from .models import User


@api_view(['POST'])
@permission_classes([AllowAny])
def sign_up(request):
    user = User(
        email=request.data.get('email'),
    )

    user.set_password(request.data.get('password'))

    try:
        user.full_clean()
    except ValidationError as errors:
        return Response(
            data=errors.error_dict,
            status=status.HTTP_400_BAD_REQUEST,
        )

    user.save()

    return Response(
        data='OK',
        status=status.HTTP_201_CREATED,
    )


@api_view(['GET'])
def sign_in(*_):
    return Response(
        data='OK',
    )
