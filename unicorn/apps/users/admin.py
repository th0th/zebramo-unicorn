from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group

from .forms import (
    UserCreationForm,
    UserUpdateForm,
)
from .models import User


# User
@admin.register(User)
class UserAdmin(BaseUserAdmin):
    form = UserUpdateForm
    add_form = UserCreationForm

    list_display = (
        'email',
    )
    list_filter = ()
    fieldsets = (
        ('Authentication', {
            'fields': (
                'email',
                'password',
            ),
        }),
    )

    add_fieldsets = (
        (None, {
            'classes': (
                'wide',
            ),
            'fields': (
                'email',
                'password1',
                'password2',
            ),
        }),
    )
    ordering = (
        'email',
    )
    filter_horizontal = ()


# Group
admin.site.unregister(Group)
