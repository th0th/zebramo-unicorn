from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'zebramo.apps.users'
    verbose_name = "Users"
